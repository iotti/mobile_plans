﻿using Microsoft.EntityFrameworkCore.Migrations;
using MySql.Data.EntityFrameworkCore.Metadata;

namespace MobilePlans.Migrations
{
    public partial class InitializeDb : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Operadoras",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Operadoras", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Planos",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    Cod = table.Column<long>(nullable: false),
                    Minutos = table.Column<int>(nullable: false),
                    Franquia = table.Column<int>(nullable: false),
                    Valor = table.Column<decimal>(nullable: false),
                    TipoPlano = table.Column<int>(nullable: false),
                    OperadoraId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Planos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Planos_Operadoras_OperadoraId",
                        column: x => x.OperadoraId,
                        principalTable: "Operadoras",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "DDDPlanos",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    DDD = table.Column<byte>(nullable: false),
                    Ativo = table.Column<bool>(nullable: false),
                    PlanoId = table.Column<long>(nullable: true),
                    OperadoraId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DDDPlanos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DDDPlanos_Operadoras_OperadoraId",
                        column: x => x.OperadoraId,
                        principalTable: "Operadoras",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DDDPlanos_Planos_PlanoId",
                        column: x => x.PlanoId,
                        principalTable: "Planos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "Operadoras",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 1L, "Vivo" },
                    { 2L, "Claro" },
                    { 3L, "Oi" },
                    { 4L, "Tim" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_DDDPlanos_Ativo",
                table: "DDDPlanos",
                column: "Ativo");

            migrationBuilder.CreateIndex(
                name: "IX_DDDPlanos_DDD",
                table: "DDDPlanos",
                column: "DDD");

            migrationBuilder.CreateIndex(
                name: "IX_DDDPlanos_Id",
                table: "DDDPlanos",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_DDDPlanos_OperadoraId",
                table: "DDDPlanos",
                column: "OperadoraId");

            migrationBuilder.CreateIndex(
                name: "IX_DDDPlanos_PlanoId",
                table: "DDDPlanos",
                column: "PlanoId");

            migrationBuilder.CreateIndex(
                name: "IX_Operadoras_Id",
                table: "Operadoras",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_Operadoras_Name",
                table: "Operadoras",
                column: "Name");

            migrationBuilder.CreateIndex(
                name: "IX_Planos_Cod",
                table: "Planos",
                column: "Cod",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Planos_Id",
                table: "Planos",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_Planos_OperadoraId",
                table: "Planos",
                column: "OperadoraId");

            migrationBuilder.CreateIndex(
                name: "IX_Planos_TipoPlano",
                table: "Planos",
                column: "TipoPlano");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DDDPlanos");

            migrationBuilder.DropTable(
                name: "Planos");

            migrationBuilder.DropTable(
                name: "Operadoras");
        }
    }
}
