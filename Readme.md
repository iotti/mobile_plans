
<h3 align="center">Busca Planos</h3>

---

<p align="center"> Aplicação simples para cadastro e busca de planos de celular
    <br> 
</p>

## 📝 Table of Contents

- [Sobre](#about)
- [Requisitos](#getting_started)
- [Swagger](#swagger)
- [Testes](#tests)

## 🧐 Sobre <a name = "about"></a>

Este repositório tem como objetivo exemplificar uma arquitetura simples para cadastro e busca de planos de operadoras de celular, através de DDD.

## 🏁 Requisitos <a name = "getting_started"></a>

Este projeto utiliza-se do .Net Core 3.0, EntityFramework Core e MySql como banco. Mas está arquitetado para suportar outros bancos relacionais com poucas mudanças.
Também está disponibilizado um [Swagger](#swagger) para testes com as rotas existentes.

### Swagger <a name = "swagger"></a>

Existe o endereço `/swagger` onde é possivel executar todas as rotas existentes na API

## ⛏️ Testes <a name = "tests"></a>


