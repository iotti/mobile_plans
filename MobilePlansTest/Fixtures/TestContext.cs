﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using System.Net.Http;
using System.Collections.Generic;
using System.Text;

namespace MobilePlansTest.Fixtures
{
    class TestContext
    {
        public HttpClient Client { get; set; }
        private TestServer _server;
        public TestContext()
        {
            SetupClient();
            Seed();
        }
        private void SetupClient()
        {
            _server = new TestServer(new WebHostBuilder().UseStartup<TestStartup>());
            Client = _server.CreateClient();
            Seed();
        }

        private async void Seed()
        {
            var response = await Client.PostAsync("api/Operadoras", new StringContent("{\"Name\":\"Vivo\"}", Encoding.UTF8, "application/json"));
            response.EnsureSuccessStatusCode();
        }
    }
}
