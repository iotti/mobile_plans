﻿using FluentAssertions;
using System.Net;
using System.Threading.Tasks;
using MobilePlansTest.Fixtures;
using Xunit;

namespace MobilePlansTest.Scenarios
{
    public class OperadoraControllerTests
    {
        private readonly TestContext _testContext;
        public OperadoraControllerTests()
        {
            _testContext = new TestContext();
        }

        [Fact]
        public async Task Values_Get_ReturnsOkResponse()
        {
            var response = await _testContext.Client.GetAsync("api/Operadoras");
            response.EnsureSuccessStatusCode();
            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }

        [Fact]
        public async Task Values_GetById_ValuesReturnsNotFoundResponse()
        {
            var response = await _testContext.Client.GetAsync("/api/Operadoras/150");
            response.StatusCode.Should().Be(HttpStatusCode.NotFound);
        }

        [Fact]
        public async Task Values_GetById_ValuesReturnsOkResponse()
        {
            var response = await _testContext.Client.GetAsync("/api/Operadoras/1");
            response.EnsureSuccessStatusCode();
            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }

        [Fact]
        public async Task Values_GetById_ReturnsBadRequestResponse()
        {
            var response = await _testContext.Client.GetAsync("/api/Operadoras/XXX");
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
        }

    }
}
