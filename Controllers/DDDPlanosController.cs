﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MobilePlans.Models;
using MobilePlans.Models.ViewModel;

namespace MobilePlans.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DDDPlanosController : ControllerBase
    {
        private readonly BaseDbContext _context;

        public DDDPlanosController(BaseDbContext context)
        {
            _context = context;
        }

        private IQueryable<DDDPlano> BuildRelationContext()
        {
            return _context.DDDPlanos.Include(p => p.Plano).ThenInclude(p => p.Operadora).Where(p => p.Ativo);
        }

        // GET: api/DDDPlanos
        [HttpGet]
        public IEnumerable<DDDPlano> GetDDDPlanos()
        {
            return BuildRelationContext().Select(x => new DDDPlanoView(x));
        }

        // GET: api/DDDPlanos/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetDDDPlano([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var DDDPlano = await BuildRelationContext().Where( o => o.Id == id).Select(x => new DDDPlanoView(x)).FirstOrDefaultAsync();

            if (DDDPlano == null)
            {
                return NotFound();
            }

            return Ok(DDDPlano);
        }

        // PUT: api/DDDPlanos/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutDDDPlano([FromRoute] long id, [FromBody] DDDPlanoCreateView DDDPlano)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (DDDPlano.Id == 0)
            {
                DDDPlano.Id = id;
            }
            CreateToDb(ref DDDPlano);

            _context.Entry(DDDPlano).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DDDPlanoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/DDDPlanos
        [HttpPost]
        public async Task<IActionResult> PostDDDPlano([FromBody] DDDPlanoCreateView DDDPlano)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            CreateToDb(ref DDDPlano);
            _context.DDDPlanos.Add(DDDPlano);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetDDDPlano", new { id = DDDPlano.Id }, DDDPlano);
        }

        // DELETE: api/DDDPlanos/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteDDDPlano([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var DDDPlano = await _context.DDDPlanos.FindAsync(id);
            if (DDDPlano == null)
            {
                return NotFound();
            }
            DDDPlano.Ativo = false;
            _context.Entry(DDDPlano).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            return Ok(DDDPlano);
        }

        private bool DDDPlanoExists(long id)
        {
            return _context.DDDPlanos.Any(e => e.Id == id);
        }

        private void CreateToDb(ref DDDPlanoCreateView dddPlano)
        {
            dddPlano.Plano = _context.Planos.Find(dddPlano.PlanoId);            
        }
    }
}