﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MobilePlans.Models;
using MobilePlans.Models.ViewModel;

namespace MobilePlans.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PlanosController : ControllerBase
    {
        private readonly BaseDbContext _context;

        public PlanosController(BaseDbContext context)
        {
            _context = context;
        }

        private IQueryable<Plano> BuildRelationContext()
        {
            return _context.Planos.Include(o => o.Operadora);
        }

        // GET: api/Planos
        [HttpGet]
        public IEnumerable<Plano> GetPlanos()
        {
            return BuildRelationContext().Select(p => new PlanoView(p));
        }

        // GET: api/Planos/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetPlano([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var plano = await BuildRelationContext().Where( p => p.Id == id).Select(p => new PlanoView(p)).FirstAsync();

            if (plano == null)
            {
                return NotFound();
            }

            return Ok(plano);
        }

        // PUT: api/Planos/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPlano([FromRoute] long id, [FromBody] PlanoCreateView plano)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            
            plano.Id = id;
            CreateToDb(ref plano);
            _context.Entry(plano).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PlanoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Planos
        [HttpPost]
        public async Task<IActionResult> PostPlano([FromBody] PlanoCreateView plano)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            CreateToDb(ref plano);
            _context.Planos.Add(plano);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetPlano", new { id = plano.Id }, plano);
        }

        // DELETE: api/Planos/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePlano([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var plano = await _context.Planos.FindAsync(id);
            if (plano == null)
            {
                return NotFound();
            }

            _context.Planos.Remove(plano);
            await _context.SaveChangesAsync();

            return Ok(plano);
        }

        private bool PlanoExists(long id)
        {
            return _context.Planos.Any(e => e.Id == id);
        }

        private void CreateToDb(ref PlanoCreateView plano)
        {
            plano.TipoPlano = (TipoPlano)Enum.Parse(typeof(TipoPlano), plano.Tipo, true);
            plano.Operadora = _context.Operadoras.Find(plano.OperadoraId);
        }
    }
}