﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MobilePlans.Models;
using MobilePlans.Models.ViewModel;

namespace MobilePlans.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BuscaController : ControllerBase
    {
        private readonly BaseDbContext _context;

        public BuscaController(BaseDbContext context)
        {
            _context = context;
        }

        protected IQueryable<DDDPlano> BuildSearch(DDDPlanoSearchView busca)
        {
            var query = _context.DDDPlanos.Include(p => p.Plano).ThenInclude(o => o.Operadora).AsQueryable();

            if (busca.Cod != null)
                query = query.Where(x => x.Plano.Cod == busca.Cod);
            if (busca.DDD != null)
                query = query.Where(x => x.DDD == busca.DDD);
            if (busca.NomeOperadora != null)
                query = query.Where(x => x.Plano.Operadora.Name.Contains(busca.NomeOperadora));
            if (busca.PlanoId != null)
                query = query.Where(x => x.Plano.Id == busca.PlanoId);
            if (busca.Tipo != null)
                query = query.Where(x => x.Plano.TipoPlano == (TipoPlano)Enum.Parse(typeof(TipoPlano), busca.Tipo, true));

            return query;
        }

        // GET: api/Busca
        [HttpGet("planos")]
        public IEnumerable<DDDPlano> GetDDDPlano([FromQuery] DDDPlanoSearchView busca)
        {
            return BuildSearch(busca).Select(x => new DDDPlanoView(x));
        }
    }
}
