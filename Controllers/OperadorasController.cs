﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MobilePlans.Models;
using MobilePlans.Models.ViewModel;

namespace MobilePlans.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OperadorasController : ControllerBase
    {
        private readonly BaseDbContext _context;

        public OperadorasController(BaseDbContext context)
        {
            _context = context;
        }

        private IQueryable<Operadora> BuildRelationContext()
        {
            return _context.Operadoras.Include(o => o.DDDPlanos).ThenInclude(d => d.Plano);
        }

        // GET: api/Operadoras
        [HttpGet]
        public IEnumerable<OperadoraView> GetOperadoras()
        {
            return BuildRelationContext().Select(o => new OperadoraView(o));
        }

        // GET: api/Operadoras/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetOperadora([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var operadora = await BuildRelationContext().Where(o => o.Id == id).Select(o => new OperadoraView(o)).FirstOrDefaultAsync();

            if (operadora == null)
            {
                return NotFound();
            }

            return Ok(operadora);
        }

        // PUT: api/Operadoras/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutOperadora([FromRoute] long id, [FromBody] OperadoraCreateView operadora)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            Operadora operadoraEdit = new Operadora { Id = id, Name = operadora.Name };
            _context.Entry(operadoraEdit).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!OperadoraExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Operadoras
        [HttpPost]
        public async Task<IActionResult> PostOperadora([FromBody] OperadoraCreateView operadora)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Operadoras.Add(operadora);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetOperadora", new { id = operadora.Id }, operadora);
        }

        // DELETE: api/Operadoras/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteOperadora([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var operadora = await _context.Operadoras.FindAsync(id);
            if (operadora == null)
            {
                return NotFound();
            }

            _context.Operadoras.Remove(operadora);
            await _context.SaveChangesAsync();

            return Ok(operadora);
        }

        private bool OperadoraExists(long id)
        {
            return _context.Operadoras.Any(e => e.Id == id);
        }
    }
}