﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.EntityFrameworkCore;
using MobilePlans.Models;

namespace MobilePlans
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        public virtual bool IsTest { get; set; } = false;

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var connectionString = Configuration.GetConnectionString("DefaultConnection");
            if(!IsTest)
                services.AddDbContext<BaseDbContext>(opt => opt.UseMySQL(connectionString));
            else
                services.AddDbContext<BaseDbContext>(opt => opt.UseInMemoryDatabase("mobile_plans_test"));
            services.AddControllers().SetCompatibilityVersion(CompatibilityVersion.Version_3_0).AddApplicationPart(typeof(Startup).Assembly);
            services.AddSwaggerDocument();
        }

        public void MigrateContext(IApplicationBuilder app)
        {
            if (IsTest)
                return;
            using (var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {                
                var context = serviceScope.ServiceProvider.GetRequiredService<BaseDbContext>();
                context.Database.Migrate();
            }

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            MigrateContext(app);
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseStaticFiles();
            app.UseRouting();
            app.UseCors();

            app.UseOpenApi();
            app.UseSwaggerUi3();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
