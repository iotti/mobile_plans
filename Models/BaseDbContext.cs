﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using MobilePlans.Models.ViewModel;

namespace MobilePlans.Models
{
    public class BaseDbContext : DbContext
    {
        public BaseDbContext(DbContextOptions<BaseDbContext> options)
           : base(options)
        {
        }
        public DbSet<Operadora> Operadoras { get; set; }
        public DbSet<Plano> Planos { get; set; }
        public DbSet<DDDPlano> DDDPlanos { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Operadora>().HasIndex(o => o.Id);
            modelBuilder.Entity<Operadora>().HasIndex(o => o.Name);
            modelBuilder.Entity<Operadora>().HasMany(o => o.DDDPlanos);
            modelBuilder.Entity<Operadora>().HasData(
                new Operadora { Id = 1, Name = "Vivo" },
                new Operadora { Id = 2, Name = "Claro" },
                new Operadora { Id = 3, Name = "Oi" },
                new Operadora { Id = 4, Name = "Tim" });

            modelBuilder.Entity<Plano>().HasIndex(p => p.Id);
            modelBuilder.Entity<Plano>().HasIndex(p => p.TipoPlano);
            modelBuilder.Entity<Plano>().HasIndex(p => p.Cod).IsUnique();
            modelBuilder.Entity<Plano>().HasOne(p => p.Operadora);
            //modelBuilder.Entity<Plano>().HasMany(p => p.DDDP);

            modelBuilder.Entity<DDDPlano>().HasIndex(op => op.Id);
            modelBuilder.Entity<DDDPlano>().HasIndex(op => op.Ativo);
            modelBuilder.Entity<DDDPlano>().HasIndex(op => op.DDD);
            modelBuilder.Entity<DDDPlano>().HasOne(op => op.Plano);
            base.OnModelCreating(modelBuilder);
        }

    }
}
