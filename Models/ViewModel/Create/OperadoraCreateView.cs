﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace MobilePlans.Models.ViewModel
{
    public class OperadoraCreateView : Operadora
    {
        [JsonIgnore]
        override public long Id { get; set; }

        [JsonIgnore]
        override public ICollection<DDDPlano> DDDPlanos { get; set; }
    }
}
