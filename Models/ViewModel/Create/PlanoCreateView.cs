﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MobilePlans.Models.ViewModel
{
    public class PlanoCreateView : Plano
    {  
        [NotMapped]
        [Required]
        public long OperadoraId { get; set; }

        [NotMapped]
        [Required]
        public String Tipo { get; set; }
    }
}
