﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace MobilePlans.Models.ViewModel
{
    public class DDDPlanoCreateView : DDDPlano
    {
        [Required]
        public long PlanoId { get; set; }
    }
}
