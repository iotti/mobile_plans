﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MobilePlans.Models.ViewModel
{
    public class OperadoraView : Operadora
    {
        public OperadoraView(Operadora o) 
        {
            if(o != null)
            {
                foreach (var prop in o.GetType().GetProperties())
                {
                    this.GetType().GetProperty(prop.Name).SetValue(this, prop.GetValue(o, null), null);
                }  
            }
                      
        }

        override public ICollection<DDDPlano> DDDPlanos { get; set; }


    }
}
