﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text.Json.Serialization;
using System.ComponentModel.DataAnnotations.Schema;

namespace MobilePlans.Models.ViewModel
{
    public class DDDPlanoView : DDDPlano
    {
        public DDDPlanoView(DDDPlano o)
        {
            if(o != null)
            {
                foreach (var prop in o.GetType().GetProperties())
                {
                    this.GetType().GetProperty(prop.Name).SetValue(this, prop.GetValue(o, null), null);
                }
            }            
        }
        [JsonIgnore]
        [NotMapped]
        override public Plano Plano { get => base.Plano; set => base.Plano = value; }
        [NotMapped]
        public PlanoView DadosPlano { get => new PlanoView(Plano); }
    }
}
