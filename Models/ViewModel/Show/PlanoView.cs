﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text.Json.Serialization;

namespace MobilePlans.Models.ViewModel
{
    public class PlanoView : Plano
    {
        public PlanoView(Plano o)
        {
            if (o != null)
            {
                foreach (var prop in o.GetType().GetProperties())
                {
                    this.GetType().GetProperty(prop.Name).SetValue(this, prop.GetValue(o, null), null);
                }
            }
            
        }
        public String Tipo
        {
            get
            {
                return TipoPlano.ToString();
            }
        }

        [JsonIgnore]
        public override TipoPlano TipoPlano { get; set; }
    }
}
