﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MobilePlans.Models.ViewModel
{
    public class DDDPlanoSearchView
    {
        public String? NomeOperadora {get; set;}
        public byte? DDD { get; set; }
        public String? Tipo { get; set; }
        public long? Cod { get; set; }
        public long? PlanoId { get; set; }

    }
}
