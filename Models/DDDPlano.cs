﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace MobilePlans.Models
{
    public class DDDPlano
    {
        public long Id { get; set; }
        [Required]
        public byte DDD { get; set; }
        public bool Ativo { get; set; } = true;        
        public virtual Plano Plano { get; set; }
    }
}
