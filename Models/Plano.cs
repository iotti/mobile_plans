﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MobilePlans.Models
{
    public enum TipoPlano
    {
        UNDEFINED = 0,
        CONTROLE = 1,
        POS = 2,
        PRE = 3
    }

    public class Plano
    {
        [Key]
        public long Id { get; set; }

        [Required]
        public long Cod { get; set; }

        [Required]
        public int Minutos { get; set; }

        [Required]
        public int Franquia { get; set; }

        [Required]
        public decimal Valor { get; set; }        
        public virtual TipoPlano TipoPlano { get; set; }
       
        public virtual Operadora Operadora { get; set; }

    }
}
