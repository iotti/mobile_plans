﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace MobilePlans.Models
{
    public class Operadora
    {
        [Key]
        public virtual long Id { get; set; }

        [Required]
        public String Name { get; set; }

        [NotMapped]
        [JsonIgnore]
        public virtual ICollection<DDDPlano> DDDPlanos { get; set; }

    }
}
